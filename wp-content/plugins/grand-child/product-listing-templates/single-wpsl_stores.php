<?php get_header(); ?>

<?php
	global $post;

	$queried_object = get_queried_object();

	$post = get_post( $queried_object->ID );
                    setup_postdata( $post );
                    the_content();
                    wp_reset_postdata( $post );

		$store_shortname       = get_post_meta( $queried_object->ID, 'wpsl_store_shortname', true );
		$address       = get_post_meta( $queried_object->ID, 'wpsl_address', true );
		$city          = get_post_meta( $queried_object->ID, 'wpsl_city', true );
		$zip          = get_post_meta( $queried_object->ID, 'wpsl_zip', true );
		$state          = get_post_meta( $queried_object->ID, 'wpsl_state', true );
		$phone       = get_post_meta( $queried_object->ID, 'wpsl_phone', true );
		$site_url       = get_post_meta( $queried_object->ID, 'wpsl_site_url', true );
		$url       = get_post_meta( $queried_object->ID, 'wpsl_url', true );
		$country       = get_post_meta( $queried_object->ID, 'wpsl_country', true );
		$destination   = $address . ',' . $city . ',' . $state.',' .$zip;
		$direction_url = "https://maps.google.com/maps?saddr=&daddr=" . urlencode( $destination ) . "";

	?>

<div class="storelocation">
	<div class="header-banner-image">
	</div>
	<div class="header-sale-image">		
		
			<?php echo do_shortcode('[coupon "salebanner" "full"]'); ?>	
		
	</div>
	<div class="container">
		<div class="row">	

				<div class="col-lg-5 col-md-6 col-sm-12 storeloc-content">
					<img src="/wp-content/uploads/2021/11/BigBobsLogo.png"/>
					
					<h1 class="entry-title sfnstoretitle"><?php echo $store_shortname;  ?></h1>
					<span class="autorized">AUTORIZED BIG BOB'S DEALER</span>   
					
					<span class="siteUrl"><a href="https://<?php echo $site_url;?>" target="_blank"><?php echo $site_url;  ?></a></span>  

					<a href="https://<?php echo $site_url;?>" target="_blank" class="fl-button" role="button">
							<span class="uabb-button-text uabb-creative-button-text">VISIT SITE</span></a>

					<div class="store_reviews">
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="fa fa-star checked"></span>
						<span class="number_review">22 REVIEWS</span>
					</div>

					<?php if($phone!=''){?>
			
					<span class="phNo">PHONE NUMBER </span>
					<span class="phNoDis"><a href="tel:<?php echo $phone;?>"><?php echo $phone;?></a></span>

					<?php } ?>
			
					<div class="storehours">
						<span>HOURS </span>
						<?php echo do_shortcode( '[wpsl_hours]' ); ?>
					</div>
			
					<div class="store_links_main">
						<span class="getLable">BIG BOB'S SERVICES </span>
						<ul class="store_services">
							<li><a href="https://<?php echo $site_url;?>/shop-at-home/" class="getdirect" target="_blank"> SHOP AT HOME</a></li>
							<li><a href="https://rugs.shop/en_us/?store=" class="getdirect" target="_blank"> BUY RUGS ONLINE</a></li>
							<li><a href="https://<?php echo $site_url;?>/request-in-home-measure/" class="button" target="_blank"> SCHEDULE A MEASURE</a></li>
						
						</ul>
					</div>
					
					<div class="store_links_main">
					<span class="getLable">PRODUCTS OFFERED</span>
						<ul class="product_offered">						
							<li><a href="https://<?php echo $site_url;?>/carpet-catalog/" class="getdirect" target="_blank">CARPET</a></li>
							<li><a href="https://<?php echo $site_url;?>/hardwood-catalog/" class="getdirect" target="_blank"> HARDWOOD</a></li>
							<li><a href="https://<?php echo $site_url;?>/vinyl-catalog/" class="getdirect" target="_blank"> VINYL</a></li>
							<li><a href="https://<?php echo $site_url;?>/laminate-catalog/" class="getdirect" target="_blank"> LAMINATE</a></li>
							<li><a href="https://<?php echo $site_url;?>/tile-catalog/" class="getdirect" target="_blank" > TILES</a></li>
							<li><a href="https://rugs.shop/en_us/?store=" class="getdirect" target="_blank"> RUGS ONLINE</a></li>
						
						</ul>	
					</div>
					
				</div>

				
				<div class="col-lg-7 col-md-6 col-sm-12 storeloc-map">
					<div class="mapcontainer">
						<div class="mapcon_main">
							<div class="mapcon_left">
								<div class="mapcon_left_flex">
								<i class="ua-icon ua-icon-location-pin" aria-hidden="true"></i>
								<div>
									<h2 class="entry-title sfnstoretitle"><?php echo $store_shortname;  ?></h2>
									<span class="add"><?php echo $destination;?></span>
								</div>
								</div>							
							</div>
							<div class="mapcon_right">
								<a href="<?php echo $direction_url ;?>" target="_blank">GET DIRECTIONS</a>
							</div>
						</div>
						
						<div class="store_map_wrap">
														
							<?php echo do_shortcode( '[wpsl_map]' ); ?>

						</div>
										
					</div>
				</div>
				
			</div>
			<div class="row">
				<div class="col-lg-12">
					<div class="bottomsection">
						<h2>About <?php echo get_the_title();?></h2>
						<p>Looking for a local flooring and carpet store in <?php echo $city; ?>? <?php echo get_the_title();?> has a wide selection of the highest quality carpet, wood, laminate, and vinyl flooring at the best prices. Conveniently located in  <?php echo $city; ?>, <?php echo $state; ?>, <?php echo get_the_title();?> has an experienced and knowledgeable staff who will guide you through each step of selecting the right flooring for your home or business. Whether you need a new carpet, wood, laminate, or vinyl, our friendly flooring experts will help you find exactly what you need to fit your taste, lifestyle, and budget. Come visit our  <?php echo $city; ?> showroom!
</p>
					</div>
					
				</div>
			</div>
	</div>
</div>

<?php get_footer(); ?>
